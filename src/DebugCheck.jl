module DebugCheck
export @dbgcheck

const DCheckDEBUG = haskey(ENV, "JULIA_DEBUG")

#-----------------------------------------------------------------------

using Test: get_test_result, do_test, Threw, Pass, Fail, Error, eval_test,
scrub_backtrace

Base.show(io::IO, t::Pass) = nothing

function Base.show(io::IO, t::Fail)
    printstyled(io, "Assert Failed"; bold=true, color=Base.error_color())
    print(io, " at ")
    printstyled(io, something(t.source.file, :none), ":", t.source.line, "\n"; bold=true, color=:default)
    print(io, "  Expression: ", t.orig_expr)
    if t.test_type == :test_throws_wrong
        # An exception was thrown, but it was of the wrong type
        print(io, "\n    Expected: ", t.data)
        print(io, "\n      Thrown: ", isa(t.data, Type) ? typeof(t.value) : t.value)
    elseif t.test_type == :test_throws_nothing
        # An exception was expected, but no exception was thrown
        print(io, "\n    Expected: ", t.data)
        print(io, "\n  No exception thrown")
    elseif t.test_type == :test && t.data !== nothing
        # The test was an expression, so display the term-by-term
        # evaluated version as well
        print(io, "\n   Evaluated: ", t.data)
    end
end

"""
`@dbgcheck ex`- like @assert but with @test like error messages
"""
macro dbgcheck(ex)
    if DCheckDEBUG
        orig_ex = Expr(:inert, ex)
        result = get_test_result(ex, __source__)
        quote
            do_test($result, $orig_ex)
        end
    end
end
end # module

#=
using Main.DebugCheck
# import Main.DebugCheck: @dbgcheck, DCheckDEBUG
# using Test: get_test_result, do_test, Threw, Pass, Fail, Error
a = 1
b = 2
@dbgcheck a == b
@dbgcheck a == a
=#
